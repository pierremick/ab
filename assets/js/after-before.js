
// assets/js/after-before.js
jQuery(document).ready(function($) {
    var $slider = $(".slider");
    var $before = $(".large-image-before");
    var $after = $(".large-image-after");
    var $legendBefore = $(".legend-before");
    var $legendAfter = $(".legend-after");
    var $afterBeforeContainer = $(".after-before-container");
    var isDragging = false;
    var isDraggingImage = false;
    var isDraggingCard = false;
    var startX = 0;
    var startY = 0;
    var sliderOffsetX = 0;
    var sliderOffsetY = 0;
    var startCardX = 0;
    var startCardY = 0;

    // Fonction pour déplacer le curseur horizontalement
    function moveSlider(offsetX) {
        var containerWidth = $before.parent().width();
        var sliderWidth = $slider.width();
        var newPositionX = $slider.position().left + offsetX;

        // Limiter la position du curseur pour qu'il reste dans les limites du conteneur
        newPositionX = Math.min(Math.max(0, newPositionX), containerWidth - sliderWidth);

        $slider.css("left", newPositionX);
    }

    // Gestionnaire d'événement pour le début du déplacement du curseur
    $slider.on("mousedown touchstart", function(e) {
        isDragging = true;
        startX = e.pageX || e.originalEvent.touches[0].pageX;
        startY = e.pageY || e.originalEvent.touches[0].pageY;
        sliderOffsetX = $slider.position().left;
        sliderOffsetY = $slider.position().top;
        e.preventDefault();
        console.log("Slider: Drag Start");
    });

    // Gestionnaire d'événement pour le début du déplacement de l'image "before"
    $before.on("mousedown touchstart", function(e) {
        isDraggingImage = true;
        startX = e.pageX || e.originalEvent.touches[0].pageX;
        startY = e.pageY || e.originalEvent.touches[0].pageY;
        e.preventDefault();
        console.log("Image: Drag Start");
    });

    // Gestionnaire d'événement pour le début du déplacement de la carte entière
    $afterBeforeContainer.on("mousedown touchstart", function(e) {
        isDraggingCard = true;
        startCardX = e.pageX || e.originalEvent.touches[0].pageX;
        startCardY = e.pageY || e.originalEvent.touches[0].pageY;
        $afterBeforeContainer.addClass("draggable");
        e.preventDefault();
        console.log("Card: Drag Start");
    });

    // Gestionnaire d'événement pour le mouvement de la souris ou du toucher
    $(document).on("mousemove touchmove", function(e) {
        // Vérifie si le curseur est actuellement en train d'être déplacé (dragging)
        if (isDragging) {
            // Calcule la différence entre la position actuelle de la souris/toucher et la position de départ
            var offsetX = (e.pageX || e.originalEvent.touches[0].pageX) - startX;
            var offsetY = (e.pageY || e.originalEvent.touches[0].pageY) - startY;
            // Appelle la fonction moveSlider pour déplacer le curseur horizontalement en fonction de l'offsetX
            moveSlider(offsetX);
            // Met à jour les coordonnées de départ pour le prochain mouvement
            startX = e.pageX || e.originalEvent.touches[0].pageX;
            startY = e.pageY || e.originalEvent.touches[0].pageY;
            console.log("Slider: Dragging");
        }
        // Vérifie si l'image "before" est actuellement en train d'être déplacée
        if (isDraggingImage) {
            // Calcule la différence entre la position actuelle de la souris/toucher et la position de départ
            var imageOffsetX = (e.pageX || e.originalEvent.touches[0].pageX) - startX;
            // Obtient la position actuelle de l'image "before" par rapport à sa gauche
            var imageCurrentOffsetX = parseFloat($before.css("left"));
            // Calcule la nouvelle position de l'image "before"
            var imageNewPositionX = imageCurrentOffsetX + imageOffsetX;
            // Calcule la limite maximale à laquelle l'image "before" peut être déplacée horizontalement
            var maxImageOffsetX = $before.parent().width() - $before.width();
            // Assure que la nouvelle position est dans les limites autorisées
            imageNewPositionX = Math.min(Math.max(0, imageNewPositionX), maxImageOffsetX);
            // Déplace l'image "before" à la nouvelle position
            $before.css("left", imageNewPositionX + "px");
            // Met à jour les coordonnées de départ pour le prochain mouvement
            startX = e.pageX || e.originalEvent.touches[0].pageX;
            console.log("Image: Dragging");
        }
        // Vérifie si la carte entière est actuellement en train d'être déplacée
        if (isDraggingCard) {
            // Calcule la différence entre la position actuelle de la souris/toucher et la position de départ
            var cardOffsetX = (e.pageX || e.originalEvent.touches[0].pageX) - startCardX;
            var cardOffsetY = (e.pageY || e.originalEvent.touches[0].pageY) - startCardY;
            // Obtient la position actuelle de défilement horizontale de la carte
            var cardCurrentOffsetX = $afterBeforeContainer.scrollLeft();
            // Obtient la position actuelle de défilement verticale de la carte
            var cardCurrentOffsetY = $afterBeforeContainer.scrollTop();
            // Déplace la carte en fonction de l'offsetX et offsetY
            $afterBeforeContainer.scrollLeft(cardCurrentOffsetX - cardOffsetX);
            $afterBeforeContainer.scrollTop(cardCurrentOffsetY - cardOffsetY);
            // Met à jour les coordonnées de départ pour le prochain mouvement
            startCardX = e.pageX || e.originalEvent.touches[0].pageX;
            startCardY = e.pageY || e.originalEvent.touches[0].pageY;
            console.log("Card: Dragging");
        }
    });

    // Gestionnaire d'événement pour la fin du déplacement
    $(document).on("mouseup touchend", function() {
        isDragging = false;
        isDraggingImage = false;
        isDraggingCard = false;
        $afterBeforeContainer.removeClass("draggable");
        console.log("Drag End");
    });

    // Gestionnaire d'événement pour le zoom avant
    $("#zoom-in").on("click", function(e) {
        e.stopPropagation(); // Empêche la propagation de l'événement au conteneur parent
        var currentScale = parseFloat($(".after-before-container img").css("transform").split(',')[3]);
        var newScale = currentScale * 1.1;
        $(".after-before-container img").css("transform", "scale(" + newScale + ")");
        console.log("Zoom In Button Clicked");
    });

    // Gestionnaire d'événement pour le zoom arrière
    $("#zoom-out").on("click", function(e) {
        e.stopPropagation(); // Empêche la propagation de l'événement au conteneur parent
        var currentScale = parseFloat($(".after-before-container img").css("transform").split(',')[3]);
        var minScale = 1.1;

        if (currentScale > minScale) {
            var newScale = currentScale * 0.9;
            $(".after-before-container img").css("transform", "scale(" + newScale + ")");
            console.log("Zoom Out Button Clicked");
        }
    });
});
